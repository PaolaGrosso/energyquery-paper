%!TEX root = complete-article.tex


\section{Architecture of Energy knowledge base} % (fold)
\label{sec:Architecture}


The Energy Knowledge Base  we developed is a centralized system which interacts with information sources distributed at different sites. It can aggregate each site's local resources information and then expose all the information to an external management system or workflow planning system (see management layer in Fig.~\ref{fig:semanticDS}). 

There are several possible scenarios for which an energy-aware management system  requires our EKB:

\begin{itemize}
\item Green path search. The management server creates a GHG-aware resource scheduling method by finding out which available resources driven by green energy sources and at how much GHG emissions .
\item Energy efficient path search. A workflow planner creates a energy efficient path to transfer a large  file from source database to destination node  across two places. It needs to know which resources on all the possible path have low power characteristics, \textit{e.g.} solid state disks, low-power processors or energy efficient ethernet switches. 
\item Workload consolidation and migration. The management system plans to consolidate  parts of the running jobs onto less nodes due to current user's elastic response time. It must identify which resources are running in low power state, are in idle state or the ones for which the power consumption is less than a threshold value. With this information, energy management system can power off candidate resources and schedule incoming jobs onto other resources.
\item Peak power management. The management system wants to know when  a node use energy most and how much energy it consume. This information can help it to create a schedule method to manage the distribution of peak power.  This information can even be used by system managers to negotiate  the price of energy transferred at some time with energy providers.
\end{itemize}
 
With this in mind we developed the EKB; Fig.~\ref{fig:ekb} shows the current architecture and its three main components: 
\begin{enumerate}
\item Resource Description Module. This module uses the INDL and EDL to describe all the necessary concepts and properties. It monitors in real-time the energy-aware resource information present in the various providers' databases. When receiving new energy information and resource information, it creates the appropriate  instances in the RDF repository. 
\item RDF repository. It stores all the RDF triples, which contains all the  templates and the instances created in the resource description module. It provides the API for inputting instance and templates. All the query services will interact with repository for the resource discovery and state information query. The query services are written in SPARQL, the RDF query language. 
\item Server.  Depending information requirements of client side, \textit{i.e.} a management server or a workflow planning system such as \cite{Zhao2010} , we define a series of query services available  to clients.  The clients can consume the services through the interfaces exposed in the server side using  the RPC protocol. The Server  contains a \textit{Query Service} component responsible for transferring the client requests to the the underlying repository.
\end{enumerate}

\begin{figure}[h]
\centering
\includegraphics[scale=0.6]{ekb}
\caption{The Architecture of the Energy Knowledge Base, showing (from bottom to top)the Resource Description Modules, the RDF repositories and the Server. }
\label{fig:ekb} 
\end{figure}

 
\subsection{Query Services}
\label{sec:query-service}
When the management system wants to query state information or discover resources,  it chooses a suitable service defined in the Server side and it sends the corresponding query parameters to it. The Server expresses the query in SPARQL using it Query Service component and it access RDF repository. The repository updates the instances on a regular basis to synchronize information with the underlying infrastructure database. This synchronization process needs the support of resource description module which is responsible for creating the instances according to EDL template. The data source come from information provider's database.

Our Query Service support now the following three query services:
\begin{enumerate}
\item ?resource?energydesc: this service returns the available resources driven by specified energy sources, resources with the low power feature and resource with other  description. For example,  query parameters could be like (CPU = 2GH, RAM =  1G, EnergySource = Solar) and (Switch = Energy Efficient Ethernet). 
\item ?resource?metrics: this service can query the energy  state of any given resources or  can discover the resources at a range of  value. For instance, this service can  return nodes that consume average power less than 200W with parameters ( CPU = 2GH, RAM = 1G, PowerAVG $ \leq$ 200Watt ).  
\item ?resource?metric?datetime: this service can search the time slot in which a specified resource in an given energy state. One parameter example is in the format of (hostname = "TWIN1", PowerAvg $ \leq$ 200Watt, Datetime = last 24 hours ).
\end{enumerate}
We also support numerical operations on the value of the various energy metrics, including  finding the maximum value and minimum value. 


\subsection{Implementation}
Fig.~\ref{fig:ekb} showed the technologies we used to the EKB components and the communication between them. The whole service is written in Java. 

External clients of our system use the RPC protocols  for remotely invoking the query service residing on the server side. XML-RPC, SOAP and REST are all possible RCP protocols: we chose XML-RPC because of it is easily understandable and it is  mature standard.  A limitation is given by the support for only few type of parameters when exchanging messaging between client and server. Still, this shortcoming  has a little impact on our EKB as we need only simple data type. 

Sesame\cite{sesame} and Jena\cite{jena16} are both frameworks for querying and analyzing RDF data. We decided to implement the EKB's repository based on the Sesame library which  provides the query API and storage API. Sesame supports storage-independent API, which allows us to switch backend without having to change the client.  

There are numerous query languages for RDF data. We compared the SPARQL\cite{sparql} with Prolog\cite{prolog}. We decided to SPARQL for RDF repository queries because of its simple syntax.  In future version of our system we might consider the Prolog language for query. Prolog is in fact able to do complicated searches and it supports inferences.


\subsection{Performance Evaluation}

We deployed the  EKB on one server of the DAS-4 cluster located at the University of Amsterdam\cite{das-4}. For testing, we assumed the DAS-4 nodes and the networking devices in this cluster were driven by different kinds of energy source.  In this cluster the provider's database  is implemented using an RRD database. This RRD database collects real time energy information from the underlying cluster resource. All the data are stored in the XML-format files. We parse date from files and put instantiated data into RDF repository.  No performance data is available in RRD database, consequently our Sesame database does not contain calculated energy metrics. 


We simulate the behavior of an external management system by sending three kinds of query from multiple clients:
\begin{itemize}
	\item Query1 is to search which resources use green source ''Solar'';
	\item Query2 is to query which resources are running with average power in the range from 100 to 200 watts;
	\item Query3 is a request of when node ''TWIN3'' uses active energy in the range (0,400 joule) with one-minute time slot. 
\end{itemize}
\textit{Query1, Query2} and \textit{Query3} are concrete examples of the three types of queries supported in our Query Service (see Sec.~\ref{sec:query-service}).

In order to test the performance of the EKB, we first evaluated the response time of the different supported queries by varying the database size, \textit{i.e.} the number of triples in Sesame, and the by changing the number of remote clients.

Our first test was to determine the average response time as function of the number of triples contained in the Sesame database. Fig.~\ref{fig:querys} shows the average value, and the standard deviation obtained in 20 subsequent runs.

We see that three types of queries have, not unexpectedly, very different response time curves. \textit{Query2} needs to search all the resources in the RDF repository, so the number of triples impacts its performance a lot. Conversely the response time of \textit{Query1} is nearly flat because of there is no impact from the number of instances. The response time of Query3 is increasing slowing with the size of triples loaded.

\begin{figure}[h]
\centering
\includegraphics[scale=0.6]{querys}
\caption{ Query Response Times for Various-size Triple-Stores }
\label{fig:querys} 
\end{figure}

We also increased the number of clients sending  simultaneous queries from 1 to 15, in order to see how the Query Service processing abilities scales. Fig.~\ref{fig:resptime-vs-clients-query1} shows that the light-weight \textit{Query1} is not sensitive to the number of clients: EKB can handle 15 simultaneous \textit{Query1} easily. Fig.~\ref{fig:resptime-vs-clients-query2} shows when more then 5 clients send \textit{Query2} at the same time, the response time steeply increases, since Sesame needs more time on processing this type of request.  We can see from Fig.~\ref{fig:resptime-vs-clients-query3} that the response time of \textit{Query3} shows stable  rising trend with the increasing number of clients.

\begin{figure} 
  \centering 
  \begin{minipage}[t]{0.45\linewidth}
    \centering 
    \includegraphics[width=1.6in]{query1.eps} 
    \caption{Query1 Response Times for Various Number of Clients}
\label{fig:resptime-vs-clients-query1} 
  \end{minipage}
  \hspace{1ex}% 
  \begin{minipage}[t]{0.45\linewidth}
    \centering 
    \includegraphics[width=1.6in]{query2.eps} 
    \caption{Query2 Response Times for Various Number of Clients}
\label{fig:resptime-vs-clients-query2}  
  \end{minipage}\\[10pt] 
  \begin{minipage}[t]{0.45\linewidth}
    \centering 
    \includegraphics[width=1.7in]{query3.eps} 
    \caption{Query3 Response Times for Various Number of Clients}
	\label{fig:resptime-vs-clients-query3} 
  \end{minipage}% 

\end{figure}





% section Architecture (end)