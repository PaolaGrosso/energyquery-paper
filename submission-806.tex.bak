%
\documentclass[10pt, conference, compsocconf]{IEEEtran}



%
\usepackage{cite}
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{subfigure} 

% *** GRAPHICS RELATED PACKAGES ***
%
\ifCLASSINFOpdf
  %\usepackage[pdftex]{graphicx}
  % declare the path(s) where your graphic files are
  % \graphicspath{{../pdf/}{../jpeg/}}
  % and their extensions so you won't have to specify these with
  % every instance of \includegraphics
  % \DeclareGraphicsExtensions{.pdf,.jpeg,.png}
\else
  % or other class option (dvipsone, dvipdf, if not using dvips). graphicx
  % will default to the driver specified in the system graphics.cfg if no
  % driver is specified.
  % \usepackage[dvips]{graphicx}
  % declare the path(s) where your graphic files are
  % \graphicspath{{../eps/}}
  % and their extensions so you won't have to specify these with
  % every instance of \includegraphics
  % \DeclareGraphicsExtensions{.eps}
\fi


% *** PDF, URL AND HYPERLINK PACKAGES ***
%
\usepackage{url}

% correct bad hyphenation here
\hyphenation{op-tical net-works semi-conduc-tor inc-lude}


\begin{document}
%
% paper title
% can use linebreaks \\ within to get better formatting as desired
\title{Semantic modelling and query for energy information in large scale infrastructure}
%\title{Towards Ontology based Energy Knowledge Base System}


% author names and affiliations
% use a multiple column layout for up to two different
% affiliations

%\author{\IEEEauthorblockN{Hao Zhu\IEEEauthorrefmark{1}\IEEEauthorrefmark{2}, Karel van der Veldt\IEEEauthorrefmark{1},Paola Grosso\IEEEauthorrefmark{1}, Zhiming Zhao\IEEEauthorrefmark{1},Xiangke Liao\IEEEauthorrefmark{2} and Cees de Laat\IEEEauthorrefmark{1}} 
%\IEEEauthorblockA{\IEEEauthorrefmark{1} System and Network Engineering research group\\
%University~of~Amsterdam\\
%Email: \{ h.zhu, karel.vd.veldt, p.grosso,z.zhao,
% delaat \} @uva.nl}  
%\IEEEauthorblockA{\IEEEauthorrefmark{2} National University of  Defense Technology
%\\Email:xkliao@nudt.edu.cn}
%}


% make the title area
\maketitle


\begin{abstract}

Including the energy profile of the computing infrastructure in the decision process for scheduling computing tasks and allocating resources is essential to improve the system energy efficiency. However, the lack of effective model of the infrastructure energy information makes it difficult for the resource managers to access up-to-date energy information of underlying environment. We model the relationship between energy components and infrastructure components and define energy description elements and energy metrics using semantic web technologies, and develop a description model called Energy Description Language (EDL). We wrap the energy monitoring components and provide a semantic based query interface for application to obtain energy information in various forms. An Energy Knowledge Base (EKB) is prototyped to provide context-based information retrieval support for applications to make energy aware decisions on scheduling and resource allocation. We validated the logic and effectiveness of the energy ontology by testing the performance the EKB.


%Management systems used in distributed high performance computing need both energy and state information of underlying resources to perform energy-aware scheduling and resources allocation. We created a resource description model called Energy Description Language (EDL) based on Semantic Web. The EDL captures the relationship between energy components and infrastructure components in a consistent manner in machine understandable descriptions; it also defines two kinds of concepts: energy description elements and energy metrics. We developed an architecture called Energy Knowledge Base (EKB) which relies on the concepts defined in the EDL to enable resource discovery and state information queries through service interfaces. We argue that this context-based information retrieval can complement management system's decision-making on scheduling and resource allocation of energy efficient management. We validated the logic and effectiveness of energy ontology by testing the performance the EKB.


\end{abstract}

\begin{IEEEkeywords}
semantic~web; ontology; energy~knowledge; energy~monitor; energy description language. 
\end{IEEEkeywords}

\IEEEpeerreviewmaketitle

\section{Introduction} % (fold)


\label{sec:introduction} 

Energy efficient resource management is becoming an increasingly important requirement for distributed environments such as Grids and Clouds. Besides cost concerns, huge energy use in data centres is beginning to prompt environmental concerns in terms of greenhouse gas (GHG) emissions. Adopting green energy and improving the energy efficiency of the computing systems are important strategies \cite{greenhost}. It is thus important to develop energy aware resource management systems for the infrastructure such as Grid and Cloud. 

%Energy efficient resources management is becoming of paramount importance, particularly for distributed systems such as Grids and Clouds. Besides cost concerns, huge energy use in data centers is beginning to prompt environmental concerns in terms of greenhouse gas (GHG) emissions. Some small and medium data centers\cite{greenhost} have already used green energy which they generate themselves or draw from a green solar or wind farm. It becomes therefore interesting to develop Grid and Cloud resources management that are capable of scheduling jobs to match available green energy supply for the purpose of cutting GHG emissions. In essence, management systems that are energy-aware when scheduling and allocating resources are promising approaches for green computing.

Strategies, such as shutting down unused nodes at night, switching nodes that have been idling for a long time to sleep mode, or scheduling latency-insensitive jobs onto nodes with green energy sources provide effective mechanisms for reducing energy consumptions of infrastructure; however, they require the management components for scheduling applications and for allocating resources not only have the information of the run time state of the infrastructure but also the energy profile on different application characteristics. The heterogeneity of resources in distributed computing infrastructures makes information representation difficult.


%Resource operators can define simple and effective methods to manage resources or schedule jobs, \textit{e.g.} shutdown at night, switching into sleep if long time idle or scheduling latency-insensitive jobs onto nodes with green energy source. To accomplish this we argue that any management system should be both energy- and infrastructure- aware. It needs to have knowledge of different resources states, \textit{e.g.} which devices are idle, how long they have already been in this state. It also needs to know which devices have low power states or energy efficient characteristics, which energy source the resources draw upon. In fact, the heterogeneity of resources present in high performance distributed computing infrastructures makes information representation difficult.

Semantic technologies, such as ontologies, pay an important role in describing distributed distributed resources. Amarnath \cite{Amarnath2009} introduces a semantic component in the conventional Grid architecture to represent the Grid metadata using ontology, and to expose the context-based information for effective resources management. This component implements an ontology description module and resource discovery module, and integrates with Grid schedulers. However, all the resource concepts in this ontology are not energy-aware and lack the capability of energy-aware information retrieval an energy-efficient management system needs. However, it is important to include power information in the resource descriptions of the infrastructure. The resource description system should provide not only the information on the energy source of resources and energy characteristics of resources, but also dynamic energy state of the infrastructure. In addition to the energy information, a suitable power model to predict future power consumption is also needed. Using the power model, a resource management system can evaluate the most energy-efficient scheduling decisions on deployment of new jobs as well as consolidation of existing jobs; it also evaluates if resource allocation for the execution of each job at each host is energy-efficient.

%In order to design an energy-aware management system for distributed systems, we need a resource description information and power model. The resource description system should provide two kinds of information: energy description information and energy state information. The former provides the information on the energy source of resources, energy characteristics of resources and so on, while state information includes the values of the different energy metrics. Besides knowing the available resources and current state of resources, an energy management system also needs an accurate power model to predict future power consumption. This power model evaluates the most energy-efficient scheduling decisions on deployment of new jobs as well as consolidation of existing jobs; it also evaluates if resource allocation for the execution of each job at each host is energy-efficient.

%The EKB works like this: an application
%need 3 node resources with CPU frequency over 2 GHz, and the number of nodes
%meet this kind of requirement exceeds 3. If management system know the energy
%source(green or brown energy) information of each node kept in the EKB. A
%greed GHG-aware resource allocation method can be created to return the 3
%nodes with least GHG emissions regardless of QoS. 

The remainder of the paper is organized as follows: Section \ref{sec:semantic} reviews the semantic web technologies
involved in EKB and proposes the Energy Description Language. Section \ref{sec:Architecture} presents the architecture of the EKB and introduces an experiment of EKB performance evaluation. Section \ref{sec:related work} describes the related work close to EKB. Finally, the future work and conclusions are finally drawn in Section \ref{sec:futurework}  respectively.



% section introduction (end)

\section{Energy knowledge base} % (fold)
A computing system is often modeled as a layered structure, for instance in \cite{Youseff2008}. In this paper, we roughly view a computing system from applications to underlying resources as four layers: applications, workflow management, knowledge representation and resources, as shown in Fig. \ref{fig:semanticDS}. 

\label{sec:semantic}
\begin{itemize} 
	\item The application layer refers to the applications which are constructed using workflow systems and the underlying resources. 
	\item The workflow management layer includes tools and components for composing, scheduling and executing application workflows. 
	\item The knowledge layer contains Semantic based descriptions of underlying resources: properties, middleware and policy configuration, which can include multiple knowledge bases \cite{Amarnath2009} \cite{Roure03thesemantic}. 
	\item the resource layer contains the infrastructure resources including computing, storage and network resources. 
\end{itemize}

\begin{figure}[ht]
\centering
\includegraphics[scale=0.4]{semanticDS}
\caption{ The Semantic Distributed System Infrastructure}
\vskip 8 pt
\label{fig:semanticDS} 
\end{figure}

Our paper mainly focuses on the knowledge layer in Fig. \ref{fig:semanticDS}. 

\subsection{Basic idea}
Energy knowledge base (EKB) in the knowledge layer contains energy information of underlying resources; it also uses the ontology describing infrastructure components, network topology and software resources. 

In our group, a schema called Infrastructure Description Language\cite{Ghijsen} was developed to model infrastructure related information, and an energy ontology is also developed to represent the relationship between energy components and infrastructure components and track some energy state. In addition to the information model, the EKB  provides the user query service to client systems, such as resource management services in workflow systems to obtain energy data. 

There are several possible scenarios for which an energy-aware management system  requires our EKB:

\begin{itemize}
\item \emph{Green path search}: search a network path only containing devices supplied by green energy sources, or search network paths with GHG emissions lower than certain value.
\item \emph{Energy efficient device selection}: select resource with low power characteristics, \textit{e.g.} solid state disks, low-power processors or energy efficient Ethernet switches when transferring a large file from source database to destination node. 
\item \emph{Workload consolidation and migration}: identify available resources with low power state to consolidate or migrate parts of the running.
\item \emph{Peak power management} in allocating resources and in negotiating the price of energy transferred at some time with energy providers.
\end{itemize}
 

%Our system relies on two semantic web based resource description languages: 
%\begin{itemize}

%\item the INDL provides
%information on the resources used in the system; it is a model for computing resources, storage resources and the network infrastructure connected them. 
%Need another line on this

%\item the EDL - Energy Description Language - is a new ontology that captures
%the relationship between energy components and infrastructure components in a
%consistent manner in machine understandable descriptions; it also defines two
%kinds of concepts: energy description elements and energy metrics.

%\end{itemize}

%The strength of our approach relies on the fact that the information expressed in ontology can support context based information retrieval; this is a power tool for energy-efficient management when
%scheduling and resource allocation. Our energy-aware resource description system exposes query service interfaces to external management systems based on query language in the semantic web. 

\subsection{Semantic web technologies}
We use the semantic web technology to build the component of Energy Knowledge Base.  There are several reasons why we chose Semantic Web technologies to create the resource description system used in our EKB. 
%The five most important ones are support for extended reasoning; coherent knowledge view; policies support; reusability and sophisticated tool support. 

Firstly, the semantic technologies such as Resources Description Framework (RDF)\cite{rdf} and Web Ontology Language (OWL)\cite{owl} enables flexible semantic or context based rather than traditional keyword based resources discovery. Moreover, the semantic technologies provide mechanisms to integrate different descriptions which are maintained by owners of different resource domains. In addition, we can create specified management policies representing specific ''situations'' and ''actions'' using Semantic Web Rule Language (SWRL)\cite{swrl}. Users can customize these management policies to meet different system's requirements on saving energy or personal preferences.  With SWRL, we can also enable automatically energy management as the information description is understandable for computer. The management system can for example determine that  individual components have values exceeding predefined threshold values (\textit{e.g.} the real-time power is over 300W) when reasoning about them through service interfaces; in this case the management system can automatically take the corresponding action to deal with this situation. Finally, the standardized semantic web technologies also promote the re-usability of the energy ontology we created. 
%At last lots of sophisticated tools and mature technologies in the semantic web area  are available to create knowledge base such as ontology editor, query language for information retrieval and so on.



\subsection{System architecture} % (fold)
\label{sec:Architecture}
The EKB; Fig.~\ref{fig:ekb} shows the current architecture and its three main components: 
\begin{enumerate}
\item Resource Description Module. This module uses the INDL and EDL to describe all the necessary concepts and properties. It monitors in real-time the energy-aware resource information present in the various providers' databases. When receiving new energy information and resource information, it creates the appropriate  instances in the RDF repository. 
\item RDF repository. It stores all the RDF triples, which contains all the  templates and the instances created in the resource description module. It provides the API for inputting instance and templates. All the query services will interact with repository for the resource discovery and state information query. The query services are written in SPARQL, the RDF query language. 
\item Query service.  Depending information requirements of client side, \textit{i.e.} a management server or a workflow planning system such as \cite{Zhao2010} , we define a series of query services available  to clients.  The clients can consume the services through the interfaces exposed in the server side using  the RPC protocol. The Server  contains a \textit{Query Service} component responsible for transferring the client requests to the the underlying repository.
\end{enumerate}


%The Energy Knowledge Base  we developed is a centralized system which interacts with information sources distributed at different sites. It can aggregate each site's local resources information and then expose all the information to an external management system or workflow planning system (see management layer in Fig.~\ref{fig:semanticDS}). 


\begin{figure}[h]
\centering
\includegraphics[scale=0.6]{ekb}
\caption{The Architecture of the Energy Knowledge Base, showing (from bottom to top): The Resource Description Modules, the RDF repositories, and the Query service. }
\label{fig:ekb} 
\end{figure}

In the next two sections, we will describe Energy Description Language and the query service respectively. 

\section{Energy Description Language}

%Fig.~\ref{fig:semanticDS} showed an knowledge layer containing two components: the energy knowledge base and the system knowledge base. 

%The infrastructure components needed by the system knowledge base are described using INDL.  It provides the concepts of physical resources as well as the virtualized resources, together with the network infrastruceture that connect these resources. We only focus the physical resources in this paper.

The Energy Description Language (EDL)\cite{energyontology} is proposed to represent energy metrics  in infrastructure and the relationships among them. This allows us to instantiate the various energy metrics and other resource descriptions that appear in distributed system domain in a uniform and highly descriptive manner. The EDL model is shown in Fig.~\ref{fig:edl}; it contains three main classes: EnergyMetric, Monitor Component and ResourceEnergyDescription.

%For the energy knowledge base we created the Energy Description Language(EDL)\cite{energyontology}, \textit{i.e.} a set of rules, classes, object-properties and data-properties that are used to represent all the heterogeneous energy metrics  and other energy-aware resource description along with the relationships among them. This allows us to instantiate the various energy metrics and other resource descriptions that appear in distributed system domain in a uniform and highly descriptive manner. The EDL model is shown in Fig.~\ref{fig:edl}; it contains three main classes: EnergyMetric, Monitor Component and ResourceEnergyDescription..

\begin{figure}[ht]
\centering
\includegraphics[scale=0.5]{edl}
\caption{The Energy Description Language - EDL and its three main parts: the EnergyMetric class and subclasses (in yellow), the Monitor Component class and its subclasse (in orange)  and the ResourceEbergyDescription and it subclasses (in gray)}
\vskip 8 pt
\label{fig:edl} 
\end{figure}

\subsection{EnergyMetric}
The \emph{EnergyMetric} defines two types of energy metric, \emph{ObservedMetric} or
\emph{CalculatedMetric}, distinguished by the way they obtained. We get the value of an
\emph{ObservedMetric} directly from reading a Power Distribution
Units(PDU) installed in the distributed system infrastructure. We obtain the value of
\emph{CalculatedMetric} by numerical calculation operation on the value of
\emph{ObservedMetric} and \emph{PerformanceMetric}. We define two types of \emph{ObservedMetric}s: \emph{PowerFactor}, \emph{ActiveEnergy} and \emph{PowerAvg}; we define also two kinds of \emph{Calculated Metric}s: \emph{EnergyEfficiency} and \emph{PowerEfficiency}.
 Users of EDL can extend this further if needed in their system.
 \\
 The \emph{CalculatedMetric} is
correlated with \emph{ObservedMetric} and \emph{PerformanceMetric} in the relationship of
\emph{InverselyProportional} and \emph{Proportional} respectively. For example, the energy efficiency of
a resource is proportional to the throughout of communication while it is determined
inverse-proportionally by active energy. The whole \emph{PerformanceMetric} object we reuse partially
from the QoS attributes in the \cite{zhao} , since EDL only focuses on the energy-aware concepts.
\emph{PowerFactor} is the ratio of the real power flowing to the load to the apparent power of one outlet.
The \emph{ActiveEnergy} and \emph{PowerAvg} mean the  energy consumption and real power consumption of one outlet. \emph{EnergyEfficiency} and \emph{PowerEfficiency} depend on the performance of resources monitored. If the resource is networking device,  throughout may be a suitable performance metric for it. Therefore, the \emph{EnergyEfficiency} and \emph{PowerEfficiency} can evaluate energy efficiency of resources when running  applications by calculating performance like throughout  per energy and power consumption unit. 
\\ 
Each \emph{EnergyMetric} object, such as \emph{PowerAvg} and \emph{EnergyEfficiency} has an energyValue modeled by datatype properties with xsd:double type value. And each \emph{EnergyMetric} object is associated with a Unit instance according to its physical quantity. In many cases a numerical value alone cannot be understood without its unit type.  

\subsection{MonitorComponent}
The \emph{PowerMeter}  object is one kind of  the energy monitor device which usually indicates a Power Distribution Unit (PDU). A PDU usually has a list of outlets that  monitor different node resources.  Each specified \emph{Outlet} instance is  responsible for informing a node resource of the observed energy metrics like average power, active energy and so forth.   Therefore, each resource  and energy metrics  are connected by a outlet. We can know which value of energy metric is produced by which resource by reasoning, and vice versa. The \emph{Datetime} class depicts the sampled  time of every observed energy metric. The \emph{SoftwareComponent} can be used to monitor the attributes we cannot get from hardware monitor such as most of performance attributes.

\subsection{Resource Energy Description}
The \emph{ResourceEnergyDesc} class represents the energy-aware description of resources besides the energy metrics we mention in the last section. In terms of its subclass, the \emph{EnergySource} class defines the kind of energy source used by the resource in the distributed system, \textit{e.g.}  wind, solar and gas. Each energy class has corresponding price of energy and the amount of GHG emissions per joule, which can be used to calculate the money cost and GHG emissions of running the resources under specified management or scheduling method. The running state of resources are determined in the \emph{PowerState} class.  As the management system should have the knowledge whether the resources is in \emph{Off},\emph{ Sleep} or \emph{Active} state. For active resource, they could be \emph{Idle} consuming power lower than \emph{Rated} power. The \emph{PowerFeature} class indicates low power characteristics resources have. Some resources are made up of  low power processor such as Atom, solid state disk and Energy Efficient Ethernet supporting IEEE 802.3az\cite{eee}.  



% section semantic (end)


 
\section{Query Service}
\label{sec:query-service}

The Query Service allows a client to query the following information:
\begin{enumerate}
\item about the available resources driven by specified energy sources, resources with the low power feature and resource with other  description. For example, query parameters could be like (CPU = 2GHz, RAM = 1GB, EnergySource = Solar) and (Switch = Energy Efficient Ethernet).
\item query the energy  state of any given resources or  can discover the resources at a range of  value. For instance, this service can  return nodes that consume average power less than 200W with parameters ( CPU = 2GHz, RAM = 1GB, PowerAVG $ \leq$ 200W).
\item search the time slot in which a specified resource in an given energy state. One parameter example is in the format of (hostname = "TWIN1", PowerAvg $ \leq$ 200W, Datetime = last 24 hours ).
\end{enumerate}
%When the management system wants to query state information or discover resources,  it chooses a suitable service defined in the Server side and it sends the corresponding query parameters to it. 

%We also support numerical operations on the value of the various energy metrics, including  finding the maximum value and minimum value. 


\subsection{Prototype}
Fig.~\ref{fig:ekb} showed the technologies we used to the EKB components and the communication between them. The whole service is written in Java. 

External clients of our system use the RPC protocols  for remotely invoking the query service residing on the server side. XML-RPC, SOAP and REST are all possible RCP protocols: we chose XML-RPC because of it is easily understandable and it is  mature standard.  A limitation is given by the support for only few type of parameters when exchanging messaging between client and server. Still, this shortcoming  has a little impact on our EKB as we need only simple data type. 

Sesame\cite{sesame} and Jena\cite{jena16} are both frameworks for querying and analyzing RDF data. We decided to implement the EKB's repository based on the Sesame library which  provides the query API and storage API. Sesame supports storage-independent API, which allows us to switch backend without having to change the client.  

There are numerous query languages for RDF data. We compared the SPARQL\cite{sparql} with Prolog\cite{prolog}. We decided to use SPARQL for RDF repository queries because of its simple syntax.  In the future we may consider Prolog for querying, as it is able to perform more complex searches and supports inferences.


\subsection{Performance characteristics}
We deployed the  EKB on one server of the DAS-4 cluster located at the University of Amsterdam\cite{das-4}. For testing purposes, we assume the DAS-4 nodes and the networking devices in this cluster are powered by different kinds of energy sources. The energy consumption of the nodes and the network devices is monitored in real-time and stored as time series in RRD database files. The EKB processes the data from the RRDs and stores the results into the RDF repository.

% We parse data from files and put instantiated data into RDF repository.  No performance data is available in RRD database, consequently our Sesame database does not contain calculated energy metrics. 

We made three trial queries to simulate the behavior of a possible EKB client:
\begin{itemize}
	\item Query1 is to search which resources use green source ''Solar'';
	\item Query2 is to query which resources are running with average power in the range from 100W to 200W;
	\item Query3 is a request of when node ''TWIN3'' uses active energy in the range (0, 400) Joule with one-minute time slot. 
\end{itemize}
%\textit{Query1, Query2} and \textit{Query3} are concrete examples of the three types of queries supported in our Query Service (see Sec.~\ref{sec:query-service}).

In order to test the performance of the EKB, we first evaluated the response time of the different supported queries by varying the database size, \textit{i.e.} the number of triples in Sesame, and the by changing the number of remote clients. Fig.~\ref{fig:querys} shows the average value, and the standard deviation obtained in 20 subsequent runs.

%Our first test was to determine the average response time as function of the number of triples contained in the Sesame database. 

We see that three types of queries perform in different response time curves. \textit{Query2} needs to search all the resources in the RDF repository, so the number of triples impacts its performance a lot. Conversely the response time of \textit{Query1} is nearly flat because of there is no impact from the number of instances. The response time of Query3 increases slowly with the number of triples.

\begin{figure}[h]
\centering
\includegraphics[scale=0.6]{querys}
\caption{ Query Response Times for Various-size Triple-Stores }
\label{fig:querys} 
\end{figure}

We also increased the number of clients sending  simultaneous queries from 1 to 15, in order to see how the Query Service processing abilities scales. Fig.~\ref{fig:resptime-vs-clients-query1} shows that the light-weight \textit{Query1} is not sensitive to the number of clients: EKB can handle 15 simultaneous \textit{Query1} easily. Fig.~\ref{fig:resptime-vs-clients-query2} shows when more then 5 clients send \textit{Query2} at the same time, the response time steeply increases, since Sesame needs more time on processing this type of request.  We can see from Fig.~\ref{fig:resptime-vs-clients-query3} that the response time of \textit{Query3} shows a stable  rising trend with the increasing number of clients.

\begin{figure} 
  \centering 
  \begin{minipage}[t]{0.45\linewidth}
    \centering 
    \includegraphics[width=1.6in]{query1.eps} 
    \caption{Query1 Response Times for Various Number of Clients}
\label{fig:resptime-vs-clients-query1} 
  \end{minipage}
  \hspace{1ex}% 
  \begin{minipage}[t]{0.45\linewidth}
    \centering 
    \includegraphics[width=1.6in]{query2.eps} 
    \caption{Query2 Response Times for Various Number of Clients}
\label{fig:resptime-vs-clients-query2}  
  \end{minipage}\\[10pt] 
  \begin{minipage}[t]{0.45\linewidth}
    \centering 
    \includegraphics[width=1.7in]{query3.eps} 
    \caption{Query3 Response Times for Various Number of Clients}
	\label{fig:resptime-vs-clients-query3} 
  \end{minipage}% 

\end{figure}





% section Architecture (end)

\section{Discussion} % (fold)

\label{sec:related work}
Several researchers have carried out work in this area and we give a brief discussion of the efforts done in Semantic Web based resource and energy management.

The authors in \cite{Daouadji2010} developed an ontology-based resources description framework for energy management purpose. By considering the energy source available, the framework allows users to identify the minimal CO2 emissions resource deployment. This CO2 aware scheduling actually is one of scenarios we support in our EKB, since it only describes resources using their energy sources and allocate them with  CO2 emissions awareness. They do not support other scenarios as we do in EKB.

\cite{Rossello-Busquet2011} and \cite{Shah} introduce Home Energy Management System which allows the users apply energy management system policies to control the devices in the home network for the purpose of reducing energy consumption. Their frameworks both include a knowledge base to provide information about devices such as their capabilities, state and network parameters. Moreover, \cite{Rossello-Busquet2011} implements rule-customized functions to apply energy management policies according to the users' priorities.
The project SESAME \cite{Fensel2010}\cite{paper-ucse2010}  uses semantic modeling and reasoning to support users in  optimizing home's energy costs. It achieves a functionality of future energy-aware management system by integrating smart metering, building automation and policy-based reasoning.  It supports the tariff dependent policies which manages devices considering various current and future pricing.

Home energy management is very different from energy management in the distributed system. They control the electrical equipments to save energy or reduce energy budget regardless of rigid QoS of application. However, the computing and communication in the distributed system should meet the requirements of QoS. Moreover, state of the resource in the distributed domain is complicated and the methods to control them are also very special comparing with home devices. It results in the different  relations and concepts kept in  two kinds of  knowledge base.  Finally, creating the policies of resource management in the distributed system needs the help of the power model and QoS model as well as information exposed by knowledge base. These models are all needed to evaluate the possible  schedule methods. The home power management instead defines a management policy just using the energy knowledge base. 
% section related work (end)

\section{Conclusions and future work}
In this paper, we presented the basic architecture of Energy Knowledge Base and discuss the description language for energy called Energy Description Language (EDL). The current prototype is based on semantic web technologies, and the system provides web service based interface for clients such as workflow planner to obtain different types of energy information. 

The research on EKB is still in its early stage, we expect to improve  EKB on the following points in the future:

\label{sec:futurework}
\begin{enumerate}

\item Application components: the EKB is now capture the relationship between energy components and resources components but overlook the application components. In fact, from the experiment in \cite{Meisner} \cite{Rajamani} we can see different kinds of applications such as CPU-bound and I/O-bound have different impact on the energy consumption of resources. Moreover, the distribution of applications even on the homogeneous resources can result in diversified energy consumption. Therefore, to increase application-aware concepts in the EKB can clearly describe the energy consumption and  provide more useful information for decision-making of management system.
\item Scalability and availability: For now the implementation of EKB is deployed on one server. This centralized resource discovery way is easy to design and manage data in the distributed system, however, it brings the poor scalability and availability due to slow respond to the resource's register and quit which depends on the information input from each provider's database. And single node failure may cause a disaster of whole network. Therefore, we plan to implement distributed EKB to improve its scalability and availability in the future. The challenge is how to deal with coordination among the different knowledge system and how to publish the information through a unified query interface.
\item Automatic energy management: As mentioned before, specified energy management method is one of  the definitions of automation actions and energy policies. If we define some situation classes and related action classes in the energy description language and then create the specified management instances, energy management can become an automatic process. Changes of resources state or available resources can trigger automation actions at the energy management system. The management system  gets this information through query service interface and finishes one specified management method. 
\end{enumerate}

 When the scheduler wants to create a schedule plan, the possible schedule plans must have knowledge of which resources are available and which control parameters and energy characteristics resources have as well as energy consumption of resources. As  current  resources information  together with power model are  inputs of predicting the energy consumption and QoS of  a plan. According to the result of prediction, scheduler can choose a best or sub-optimal plan. EKB is designed to provide this knowledge of current resource information, with a capability of context based energy aware information retrieval. The core of EKB is an energy description language which expresses all the energy information in the distributed system in a machine understandable format. According the possible scenarios the EKB interacted with management system, we define a number of services  for resource discovery and  the state information query.
% section future work (end)


% use section* for acknowledgement
\section*{Acknowledgment}

This work was supported by NWO through the GreenClouds project.
This publication was also supported by the Dutch national program COMMIT.


%\bibliographystyle{IEEEtran}
\bibliographystyle{unsrt}
\bibliography{eqLib}
\end{document}


